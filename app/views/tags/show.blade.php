@extends('layouts.bootstrap')

@section('content')

@include('layouts.navbar', array('resource' => $resource))
<h1>Showing {{ $item }}</h1>

<div class="jumbotron text-center">
    <h2>{{ $item }}</h2>
    <p>
        <strong>Title:</strong> {{ $item->title }}<br>
    </p>
</div>
@stop