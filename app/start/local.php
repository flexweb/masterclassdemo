<?php
Auth::loginUsingId(1);

// include "events.php";

Event::listen(
    'post.created',
    function ($post) {
        $event = appFactory::createPostEvent($post, 'post.created');
        $event->save();
    }
);

Event::listen(
    'post.updated',
    function ($post) {
        $event = appFactory::createPostEvent($post, 'post.updated');
        $event->save();
    }
);
