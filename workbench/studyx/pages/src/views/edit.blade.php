@extends('layouts.bootstrap')

@section('content')

@include('layouts.navbar', array('resource' => $resource))
<h1>Edit {{ $item }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($item, array('route' => array($resource . '.update', $item->id), 'method' => 'PUT')) }}

<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('content', 'Content') }}
    {{ Form::text('content', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('active', 'Active') }}
    {{ Form::text('active', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('published', 'Published') }}
    {{ Form::text('published', null, array('class' => 'form-control')) }}
</div>
{{ Form::submit('Edit the ' . ucfirst($resource) . '!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop