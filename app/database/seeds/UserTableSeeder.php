<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 3-5-14
 * Time: 15:14
 */

class UserTableSeeder extends Seeder
{
    const ADMIN_NAME = 'Administrator';
    const ADMIN_EMAIL = 'harry@studyx.be';
    const ADMIN_PASSWORD = 'secret';

    public function run()
    {
        Author::where('user_id', '>', 0)->update(array('user_id' => 0));

        DB::table('users')->delete();

        User::create(
            array(
                'name' => self::ADMIN_NAME,
                'email' => self::ADMIN_EMAIL,
                'root' => Role::ROOT,
                'role' => Role::ROLE_ADMIN,
                'password' => Hash::make(self::ADMIN_PASSWORD)
            )
        );

        User::create(
            array(
                'name' => 'Harry van der Valk',
                'email' => 'harryvandervalk@studyx.be',
                'password' => Hash::make('secret')
            )
        );

        User::create(
            array(
                'name' => 'Your name',
                'email' => 'your_email@studyx.be',
                'password' => Hash::make('secret')
            )
        );

        $this->command->info('A few users are created!');

    }
} 