<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 3-5-14
 * Time: 15:13
 */

class AuthorTableSeeder extends Seeder
{
    //const USER_ID = 1;

    public function run()
    {
        DB::table('posts')->delete();
        DB::table('authors')->delete();

        //$user = User::find(self::USER_ID);

        $user = DB::table('users')->where('root', true)->where('role', Role::ROLE_ADMIN)->first();

        if ($user) {
            $author = Author::create(array('title' => 'Harry van der Valk', 'email' => $user->email, 'user_id' => $user->id));
            Post::create(array('title' => 'PHP is koel', 'content' => 'Lordem Ipsem ...', 'author_id' => $author->id));
            Post::create(array('title' => 'PHP is super', 'content' => 'Lordem Ipsem ...', 'author_id' => $author->id));
            Post::create(array('title' => '.NET is super gaaf', 'content' => 'Lordem Ipsem ...', 'author_id' => $author->id));
        }

        $this->command->info('User post are created!');
    }
} 