<?php
/**
 * Created by PhpStorm.
 * User: Harry van der Valk @ HVSoftware
 * Date: 3-5-14
 * Time: 15:13
 */

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        // find the category or create it into the database
        Category::firstOrCreate(array('title' => 'PHP'));
        Category::firstOrCreate(array('title' => '.NET'));
        Category::firstOrCreate(array('title' => 'JAVA'));
        Category::firstOrCreate(array('title' => 'UNKNOWN'));

//        $category = new Category();
//        $category->title = 'NULL';
//        $category->save();

        // find the category or instantiate a new instance into the object we want
        $category = Category::firstOrNew(array('title' => 'Delphi'));
    }
} 