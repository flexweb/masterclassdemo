<?php

Route::group(
    array('prefix' => 'admin', 'before' => 'auth'),
    function () {
        Route::get(
            '/',
            array(
                'as' => 'admin.home',
                function () {
                    return View::make('admin.home');
                }
            )
        );
    }
);