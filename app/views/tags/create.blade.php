@extends('layouts.bootstrap')

@section('content')

@include('layouts.navbar', array('resource' => $resource))

<h1>Create a {{ ucfirst($resource) }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => $resource)) }}

<div class="form-group">
    {{ Form::label('title', 'Title') }}
    {{ Form::text('title', Input::old('name'), array('class' => 'form-control')) }}
</div>

{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop